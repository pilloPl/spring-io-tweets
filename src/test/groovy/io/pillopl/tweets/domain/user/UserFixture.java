package io.pillopl.tweets.domain.user;



import io.pillopl.tweets.domain.user.User;

import static java.time.Instant.now;

public class UserFixture {

    public static User anUser() {
        return anUser("John");
    }

    public static User anUser(String screenName) {
        return new User(screenName);
    }


}
