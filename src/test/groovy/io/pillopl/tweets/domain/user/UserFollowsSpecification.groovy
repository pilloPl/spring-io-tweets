package io.pillopl.tweets.domain.user

import spock.lang.Specification

class UserFollowsSpecification extends Specification {

    def 'should successfully add a new followee'() {
        given:
            User john = UserFixture.anUser("John")
        and:
            User adrian = UserFixture.anUser("Adrian")
        when:
            john.follow(adrian)
        then:
            john.follows(adrian)
    }


    def 'should successfully remove the followee'() {
        given:
            User john = UserFixture.anUser("John")
            User adrian = UserFixture.anUser("Adrian")
        and:
            john.follow(adrian)
        when:
            john.unfollow(adrian)
        then:
            !john.follows(adrian)
    }
}
