package io.pillopl.tweets.integration

import io.pillopl.tweets.boundary.tweets.DoTweet
import io.pillopl.tweets.boundary.tweets.TweetsController
import io.pillopl.tweets.domain.user.User
import io.pillopl.tweets.domain.user.UserRepository
import org.springframework.beans.factory.annotation.Autowired

import static io.pillopl.tweets.domain.user.UserFixture.anUser

public class TweetsIntegrationSpec extends IntegrationSpec {

    @Autowired TweetsController tweetsController

    @Autowired UserRepository userRepository

    def 'should remember that someone tweeted something'() {
        given:
            User john =  userRepository.save(anUser("john"))
        when:
            tweetsController.tweet(new DoTweet(john.id, "a new tweet!"))
        then:
            john.tweets.size() == 1
    }


}
