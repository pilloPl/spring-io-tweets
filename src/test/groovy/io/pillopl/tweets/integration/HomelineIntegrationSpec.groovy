package io.pillopl.tweets.integration

import io.pillopl.tweets.boundary.followers.Follow
import io.pillopl.tweets.boundary.followers.FollowersController
import io.pillopl.tweets.boundary.homeline.HomelineController
import io.pillopl.tweets.boundary.tweets.DoTweet
import io.pillopl.tweets.boundary.tweets.TweetsController
import io.pillopl.tweets.domain.user.User
import io.pillopl.tweets.domain.user.UserFixture
import io.pillopl.tweets.domain.user.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Ignore

import javax.persistence.EntityManager

public class HomelineIntegrationSpec extends IntegrationSpec {

    @Autowired FollowersController followersController

    @Autowired TweetsController tweetsController

    @Autowired UserRepository userRepository

    @Autowired HomelineController homelineController

    @Autowired EntityManager entityManager

    @Ignore
    def 'should load homeline'() {
        given:
            User john = anUser("john")
            User adrian = anUser("adrian")
            User cristian = anUser("cristian")
        and:
            userFollows(john, adrian)
            userFollows(john, cristian)
        when:
            userTweets(adrian, "1st tweet of adrian")
            userTweets(adrian, "2nd tweet of adrian")
            userTweets(cristian, "1st tweet of cristian")

        then:
            homelineController.load(john.id).size() == 3
            homelineController.load(adrian.id).size() == 0

    }

    void userTweets(User adrian, String tweet) {
        tweetsController.tweet(new DoTweet(adrian.id, tweet))
        entityManager.flush()
    }

    void userFollows(User john, User adrian) {
        followersController.follow(new Follow(john.id, adrian.id))
        entityManager.flush()
    }

    User anUser(String name) {
        return userRepository.save( UserFixture.anUser(name));
    }

}
