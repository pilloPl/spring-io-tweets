package io.pillopl.tweets.boundary.followers;

import io.pillopl.tweets.domain.user.FollowersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("/followers")
public class FollowersController {

    private final FollowersService followersService;

    @Autowired
    FollowersController(FollowersService followersService) {
        this.followersService = followersService;
    }

    @PutMapping
    public ResponseEntity follow(@RequestBody Follow follow) {
        followersService.follow(follow.getUserId(), follow.getNewFollowee());
        return ResponseEntity.ok().build();
    }

    @DeleteMapping
    public ResponseEntity unfollow(@RequestBody Unfollow unfollow) {
        followersService.unfollow(unfollow.getUserId(), unfollow.getToUnfollowId());
        return ResponseEntity.ok().build();
    }
}

class Unfollow {

    private final Long userId;
    private final Long toUnfollowId;

    Unfollow(Long userId, Long toUnfollowId) {
        this.userId = userId;
        this.toUnfollowId = toUnfollowId;
    }

    Long getToUnfollowId() {
        return toUnfollowId;
    }

    Long getUserId() {
        return userId;
    }
}


class Follow {

    private final Long userId;
    private final Long newFollowee;

    Follow(Long userId, Long newFollowee) {
        this.userId = userId;
        this.newFollowee = newFollowee;
    }

    Long getUserId() {
        return userId;
    }

    Long getNewFollowee() {
        return newFollowee;
    }
}

