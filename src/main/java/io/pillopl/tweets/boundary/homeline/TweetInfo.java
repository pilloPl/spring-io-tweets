package io.pillopl.tweets.boundary.homeline;

import io.pillopl.tweets.domain.tweets.Tweet;

import java.time.Instant;

public class TweetInfo {

    private final String body;
    private final Instant when;
    private final Long authorId;

    TweetInfo(String body, Instant when, Long authorId) {
        this.body = body;
        this.when = when;
        this.authorId = authorId;
    }

    String getBody() {
        return body;
    }

    Instant getWhen() {
        return when;
    }

    Long getAuthorId() {
        return authorId;
    }

    public static TweetInfo from(Tweet tweet) {
        return new TweetInfo(tweet.getBody(), tweet.getWhen(), tweet.getAuthorId());
    }
}
