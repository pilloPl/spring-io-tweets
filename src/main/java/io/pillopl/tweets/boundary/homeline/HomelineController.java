package io.pillopl.tweets.boundary.homeline;

import io.pillopl.tweets.domain.homeline.HomelineService;
import io.pillopl.tweets.domain.tweets.Tweet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
class HomelineController {

    private final HomelineService homelineService;

    @Autowired
    HomelineController(HomelineService homelineService) {
        this.homelineService = homelineService;
    }

    @GetMapping("/homeline/{userId}")
    public List<TweetInfo> load(@PathVariable Long userId) {
        return homelineService.homeline(userId)
                .stream()
                .map(TweetInfo::from)
                .collect(Collectors.toList());
    }



}


