package io.pillopl.tweets.boundary.tweets;

import io.pillopl.tweets.domain.tweets.TweetsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController("/tweets")
public class TweetsController {

    private final TweetsService tweetsService;

    public TweetsController(TweetsService tweetsService) {
        this.tweetsService = tweetsService;
    }

    @PostMapping
    public ResponseEntity tweet(@RequestBody DoTweet doTweet) {
        tweetsService.tweet(doTweet.getUserId(), doTweet.getText());
        return ResponseEntity.ok().build();
    }

}

class DoTweet {

    private final Long userId;
    private final String text;

    DoTweet(Long userId, String text) {
        this.userId = userId;
        this.text = text;
    }

    public Long getUserId() {
        return userId;
    }

    public String getText() {
        return text;
    }
}