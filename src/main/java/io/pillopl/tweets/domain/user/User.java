package io.pillopl.tweets.domain.user;

import io.pillopl.tweets.domain.tweets.Tweet;

import javax.persistence.*;
import java.util.*;

@Entity(name = "users")
public class User {

    @Id
    @GeneratedValue(generator = "users_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "users_seq", sequenceName = "users_seq", allocationSize = 1)
    private Long id;

    @Column(name = "screen_name", nullable = false, length = 36)
    private String screenName;

    @Version
    @Column(nullable = false)
    private long version;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<FollowedUser> followees = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Tweet> tweets = new ArrayList<>();

    User(String screenName) {
        this.screenName = screenName;
    }

    private User() {
    }

    public void tweet(String text) {
        tweets.add(new Tweet(text, id));
    }

    public void follow(User followee) {
        followees.add(new FollowedUser(id, followee));
    }

    public void unfollow(User followee) {
        possibleFollowee(followee)
                .ifPresent(followees::remove);
    }

    boolean follows(User anUser) {
        return possibleFollowee(anUser).isPresent();
    }

    private Optional<FollowedUser> possibleFollowee(User user) {
        return followees
                .stream()
                .filter(toUnfollow -> toUnfollow.hasScreenName(user.getScreenName()))
                .findAny();
    }

    public String getScreenName() {
        return screenName;
    }

    public Long getId() {
        return id;
    }

    public Set<FollowedUser> followees() {
        return Collections.unmodifiableSet(followees);
    }

    public List<Tweet> tweets() {
        return Collections.unmodifiableList(tweets);
    }
}
