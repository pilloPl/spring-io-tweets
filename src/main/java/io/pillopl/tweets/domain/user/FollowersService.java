package io.pillopl.tweets.domain.user;

import io.pillopl.tweets.domain.user.User;
import io.pillopl.tweets.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class FollowersService {

    private final UserRepository userRepository;

    @Autowired
    public FollowersService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void follow(Long followerId, Long followeeId) {
        User follower = userRepository.findOne(followerId);
        User followee = userRepository.findOne(followeeId);
        follower.follow(followee);
    }

    public void unfollow(Long followerId, Long followeeId) {
        User follower = userRepository.findOne(followerId);
        User followee = userRepository.findOne(followeeId);
        follower.unfollow(followee);
    }
}
