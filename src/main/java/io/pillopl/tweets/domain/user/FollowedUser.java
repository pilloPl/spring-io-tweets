package io.pillopl.tweets.domain.user;

import io.pillopl.tweets.domain.tweets.Tweet;

import javax.persistence.*;
import java.util.List;

@Entity(name = "followers")
public class FollowedUser {

    @Id
    @GeneratedValue(generator = "followers_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "followers_seq", sequenceName = "followers_seq", allocationSize = 1)
    private Long id;

    private Long followerId;

    @OneToOne
    private User followee;

    FollowedUser(Long followerId, User followee) {
        this.followerId = followerId;
        this.followee = followee;
    }

    private FollowedUser() {
    }

    boolean hasScreenName(String screenName) {
        return screenName.equals(followee.getScreenName());
    }

    public List<Tweet> tweets() {
        return followee.tweets();
    }

}
