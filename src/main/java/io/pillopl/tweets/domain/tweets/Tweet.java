package io.pillopl.tweets.domain.tweets;

import javax.persistence.*;
import java.time.Instant;

@Entity(name = "tweets")
public class Tweet {

    @Id
    @GeneratedValue(generator = "tweets_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "tweets_seq", sequenceName = "tweets_seq", allocationSize = 1)
    private Long id;

    @Column(nullable = false, length = 600)
    private String body;

    @Column(nullable = false, name = "occurred_at")
    private Instant occurredAt = Instant.now();

    @Column(nullable = false)
    private Long author;

    public Tweet(String body, Long author) {
        this.body = body;
        this.author = author;
    }

    private Tweet() {
    }

    public String getBody() {
        return body;
    }

    public Instant getWhen() {
        return occurredAt;
    }

    public Long getAuthorId() {
        return author;
    }
}
