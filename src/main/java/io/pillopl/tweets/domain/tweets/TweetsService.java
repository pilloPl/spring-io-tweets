package io.pillopl.tweets.domain.tweets;

import io.pillopl.tweets.domain.user.User;
import io.pillopl.tweets.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class TweetsService {

    private final UserRepository userRepository;

    @Autowired
    public TweetsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void tweet(Long authorId, String text) {
        User author = userRepository.findOne(authorId);
        author.tweet(text);
    }

}
