package io.pillopl.tweets.domain.homeline;

import io.pillopl.tweets.domain.user.FollowedUser;
import io.pillopl.tweets.domain.tweets.Tweet;
import io.pillopl.tweets.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class HomelineService {

    private final UserRepository userRepository;

    @Autowired
    public HomelineService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<Tweet> homeline(Long userId) {
        //TODO impl
        return Collections.emptyList();    }


}
